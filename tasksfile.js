const { sh, cli } = require('tasksfile');
const path = require('path');

function analyze(options, ...args) {
  const canSupplement =
    'GITHUB_REF' in process.env && 'GITHUB_EVENT_NAME' in process.env && 'GITHUB_EVENT_PATH' in process.env;
  if (canSupplement === true) {
    console.log('trying to supplement for a better analysis report..');
    const type = process.env.GITHUB_EVENT_NAME;
    const info = require(path.resolve(process.env.GITHUB_EVENT_PATH));
    const [, head] = /^refs\/heads\/(.+)/.exec(process.env.GITHUB_REF);

    switch (type) {
      case 'pull_request':
        args.push(`-Dsonar.pullrequest.base=${info.pull_request.base.ref}`);
        args.push(`-Dsonar.pullrequest.branch=${head}`);
        args.push(`-Dsonar.pullrequest.key=${info.number}`);
        break;
      case 'push':
        args.push(`-Dsonar.branch.name=${head}`);
        break;
    }
  }

  const command = `sonar-scanner ${args.join(' ')}`;
  sh(command);
}

cli({ analyze });
