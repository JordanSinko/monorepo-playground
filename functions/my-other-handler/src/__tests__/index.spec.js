const { handler } = require('..');

test('given a request; when its contains no name; then it should use default greeting', async () => {
  const actual = await handler({});
  expect(actual).toEqual(`Hello from other Lambda`);
});
