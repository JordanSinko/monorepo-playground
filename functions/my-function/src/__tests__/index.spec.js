const { handler } = require('..');

test('given a request; when its contains no name; then it should use default greeting', async () => {
  const actual = await handler({});
  expect(actual).toEqual('Hello world!');
});

test('given a request; when its contains a name; then it should greet them', async () => {
  const actual = await handler({ name: 'Jordan' });
  expect(actual).toEqual('Hello Jordan!');
});
