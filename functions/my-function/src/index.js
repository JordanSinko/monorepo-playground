const { struct } = require('superstruct');

const Event = struct({ name: 'string?' }, { name: 'world' });

exports.handler = event => {
  const [, validated] = Event.validate(event);
  return `Hello ${validated.name}!`;
};
