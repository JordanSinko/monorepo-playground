#!/bin/sh -l

export GITHUB_PULL_REQUEST_NUMBER=$(jq --raw-output .number "$GITHUB_EVENT_PATH")
export GITHUB_PULL_REQUEST_HEAD=$(jq --raw-output .pullrequest.head.ref "$GITHUB_EVENT_PATH")
export GITHUB_PULL_REQUEST_BASE=$(jq --raw-output .pullrequest.base.ref "$GITHUB_EVENT_PATH")

printenv