workflow "Push Workflow" {
    on       = "push"

    resolves = [
        "push.github.release"
    ]
}

action "push.filter.master" {
    uses = "actions/bin/filter@master"
    args = "branch master"
}

action "push.npm.install" {
    needs = "push.filter.master"
    uses  = "actions/npm@master"
    runs  = "npm ci"
}

action "push.npm.bootstrap" {
    needs = "push.npm.install"
    uses  = "actions/npm@master"
    runs  = "npx lerna bootstrap --hoist"
}

action "push.npm.test" {
    needs = "push.npm.bootstrap"
    uses  = "actions/npm@master"
    runs  = "npx jest --coverage"
}

action "push.npm.analyze.sonar" {
    needs   = "push.npm.test"
    uses    = "actions/npm@master"
    runs    = "npx task analyze -Dsonar.login=$SONAR_TOKEN"

    secrets = [
        "SONAR_TOKEN"
    ]
}

action "push.npm.analyze.lgtm" {
    uses    = "actions/bin/curl@master"
    args    = "-X POST 'https://lgtm.com/api/v1.0/analyses/1508996726369?commit=$GITHUB_SHA' -H 'Accept: application/json' -H 'Authorization: Bearer $LGTM_TOKEN'"

    secrets = [
        "LGTM_TOKEN"
    ]
}

action "push.github.release" {
    uses    = "actions/npm@master"
    runs    = "npx semantic-release"

    needs   = [
        "push.npm.analyze.lgtm",
        "push.npm.analyze.sonar"
    ]

    secrets = [
        "GH_TOKEN"
    ]
}

workflow "Pull Workflow" {
    on       = "pull_request"

    resolves = [
        "pull.npm.analyze",
    ]
}

action "pull.npm.install" {
    uses = "actions/npm@master"
    runs = "npm ci"
}

action "pull.npm.bootstrap" {
    needs = "pull.npm.install"
    uses  = "actions/npm@master"
    runs  = "npx lerna bootstrap --hoist"
}

action "pull.npm.test" {
    needs = "pull.npm.bootstrap"
    uses  = "actions/npm@master"
    runs  = "npx jest --coverage"
}

action "pull.npm.analyze" {
    needs   = "pull.npm.test"
    uses    = "actions/npm@master"
    runs    = "npx task analyze -Dsonar.login=$SONAR_TOKEN"

    secrets = [
        "SONAR_TOKEN"
    ]
}
